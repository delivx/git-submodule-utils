#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const { spawnSync } = require('child_process');

// Load package.json
const INSTALL_DIRECTORY = process.cwd();
const PACKAGE_FILE = path.join(INSTALL_DIRECTORY, 'package.json');
const packageInfo = require(PACKAGE_FILE);

// Get command line arguments
const [,, selectedSubmodulePath, newBranchName, ...args] = process.argv;

if (selectedSubmodulePath && newBranchName) {
  if (packageInfo.submodules && Array.isArray(packageInfo.submodules) && packageInfo.submodules.length) {
    const submoduleConfig = packageInfo.submodules.find(submoduleConfig => submoduleConfig.path === selectedSubmodulePath);
    if (submoduleConfig) {
      if (submoduleConfig.branch !== newBranchName) {
        const submodulePath = path.join(INSTALL_DIRECTORY, selectedSubmodulePath);

        // Checkout proper branch
        let branchCheckoutProcess = spawnSync('git', ['checkout', newBranchName], { stdio: 'inherit', cwd: submodulePath });

        // Try fetching from origin
        if (branchCheckoutProcess.status) {
          spawnSync('git', ['fetch'], { stdio: 'inherit', cwd: submodulePath });
          branchCheckoutProcess = spawnSync('git', ['checkout', newBranchName], { stdio: 'inherit', cwd: submodulePath });
        }

        if (!branchCheckoutProcess.status) {
          // Save branch config
          submoduleConfig.branch = newBranchName;
          fs.writeFileSync(PACKAGE_FILE, JSON.stringify(packageInfo, null, 2));

          // Pull changes from origin
          spawnSync('git', ['pull', 'origin', newBranchName], { stdio: 'inherit', cwd: submodulePath });

          // Install branch packages
          spawnSync('npm', ['install'], { stdio: 'inherit', cwd: submodulePath });
        } else {
          console.log(`Submodule does not have "${newBranchName}" branch`);
        }
      } else {
        console.error(`Submodule already on branch "${newBranchName}"`);
      }
    } else {
      console.error(`Submodule at path ${selectedSubmodulePath} not found!`);
    }
  }
} else {
  console.log('Usage: git-submodule-branch <submodule-path> <new-branch-name>');
}
