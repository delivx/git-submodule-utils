# Git Submodule Utils

This small package is meant to help initializing git submodules, and pinning
them to a certain branch instead of a certain commit.

To set up your repository, first install `git-submodule-utils` as a dev
dependency:

```
npm install --save-dev git-submodule-utils
```

Then, add a postinstall script to your `package.json` file, and define your
submodules

```
{
  //...,
  "scripts": {
    "postinstall": "./node_modules/.bin/git-submodule-init"
  },
  "submodules": [
    {
      "path": "my-submodule-path",
      "url": "my-submodule-git-repository",
      "branch": "master"
    }
  ]
}
```

You can update your submodules any time in the future, and run `npm install` or
`npm run postinstall` to reinitialize them.

If you want to change the branch of your submodule, you can use this script to
spare some time with checking out the proper branch and installing it's npm
dependencies:

```
./node_modules/bin/git-submodule-branch my-submodule-path branch-to-change-to
```
